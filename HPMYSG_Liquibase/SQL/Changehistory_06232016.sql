--liquibase formatted sql
--changeset  Murugan-Dev User:SQLHPMYSG_06232016_1

IF EXISTS (select object_id from sys.objects where type='P' and name='SQLTestCreateDeepLink_P') DROP PROCEDURE SQLTestCreateDeepLink_P
GO

CREATE PROCEDURE [dbo].[SQLTestCreateDeepLink_P]
(
  @link_GUID          varchar(100),
  @target_link        varchar(50), 
  @target_link_iframe varchar(200), 
  @ExpireDate         date, 
  @Disable            bit
)

AS
BEGIN
SET NOCOUNT ON 

BEGIN TRY 
BEGIN TRANSACTION

Declare	@ID int
select @ID=ID from DeepLinks where link_GUID=LTRIM(RTRIM(@link_GUID))

--Added by  murugan
IF (@link_GUID IS NOT NULL and @link_GUID<>'')
BEGIN
 IF @ID IS NULL
 BEGIN
  insert into DeepLinks(link_GUID,target_link,target_link_iframe,ExpireDate,Disable) values (@link_GUID,@target_link,@target_link_iframe,@ExpireDate,@Disable)
 END
END
COMMIT TRANSACTION
END TRY 

BEGIN CATCH 
  IF @@TRANCOUNT>0

  ROLLBACK TRANSACTION

      Declare @ErrorMessage varchar(1000),@ErrorSeverity int,@ErrorState int
      SELECT @ErrorMessage=ERROR_MESSAGE(),@ErrorSeverity=ERROR_SEVERITY(),@ErrorState=ERROR_STATE()
      RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)

END CATCH 

END
GO

--rollback drop procedure SQLTestCreateDeepLink_P

IF EXISTS (select object_id from sys.objects where type='P' and name='SQLTestGetAssociatedSKUsByAssetId') DROP PROCEDURE SQLTestGetAssociatedSKUsByAssetId
GO

CREATE PROCEDURE [dbo].[SQLTestGetAssociatedSKUsByAssetId]
	@assetId varchar(max),
	@baseUrl varchar(max),
	@langCode varchar(max),
	@assetType varchar(max)
AS


declare @localeId int

select @localeId = localeId from [HP_CAPS_DAILY].[dbo].Locale where internalName = @langCode
set @localeId = coalesce(@localeId,3)

if @assetType = 'asset' begin
	
	SELECT distinct
		[Limit1].[asset_category] AS [category], 
		[Limit1].[sku] AS [sku], 
		[Limit1].[url] as [url],
		[Limit1].[html_script] AS [html_script],
		'Asset' as [type]
		
		FROM (
			SELECT TOP (10) 
				[a].[asset_category] AS [asset_category],

				replace(
				replace(
				replace(
				replace(
				replace(
				replace([a].[HTML_SCRIPT], 'http://download.', 'https://download.')
				,'http://c.brightcove.com/', 'https://secure.brightcove.com/')
				,'http://admin.brightcove.com/', 'https://sadmin.brightcove.com/')
				,'<param name="allowScriptAccess" value="always" />', '<param name="allowScriptAccess" value="always" /><param name="secureConnections" value="true" /><param name="secureHTMLConnections" value="true" />')
				,'allowScriptAccess="always" ', 'allowScriptAccess="always" secureConnections="true" secureHTMLConnections="true" ')
				,'http://www.macromedia.com/', 'https://www.macromedia.com/')
				as [HTML_SCRIPT],
	
				N'<a/ target=''_blank'' href=' + @baseUrl +
				CASE
					WHEN ([i].[product_type_pmoid] IN (12771,329290)) THEN N'/#/ipg/product/'
					ELSE N'/#/ipg/product/'
				END + [i].[number] + N'>' + [i].[number] + N'</a>' AS [sku],
	
				a.asset_embed_code as url
		
			FROM  [HP_CAPS_DAILY].[dbo].[asset] [a]
			join [HP_CAPS_DAILY].[dbo].[itemtoasset] ita on a.AssetID = ita.AssetID
			JOIN [HP_CAPS_DAILY].[dbo].[item] [i] ON [ita].[ItemID] = [i].[ItemID]
			left join hp_caps_daily.dbo.BusinessGroupToItem bgti on (bgti.itemid = i.itemid)
			WHERE ([a].[asset_id] = @assetId) AND ([ita].[LocaleID] = @localeId) and i.itemlevel = 7
			order by case when bgti.itemid is not null then 1 else 2 end, i.full_date desc
		)  AS [Limit1]
end 
else begin
	SELECT distinct
		[Limit1].[document_type] AS [category], 
		[Limit1].[sku] AS [sku], 
		[Limit1].[url] AS [url],
		'' as [html_script],
		'Document' as [type]
		FROM ( SELECT TOP (10) 
			[d].[document_type] AS [document_type], 
			[d].[url] AS [url], 
			N'<a/ target=''_blank'' href=' + @baseUrl + 
				CASE 
					WHEN ([i].[product_type_pmoid] IN (18972,15179)) THEN N'/#/ipg/product/' 
					ELSE N'/#/ipg/product/' 
				END + [i].[number] + N'>' + [i].[number] + N'</a>' AS [sku]
			FROM  [HP_CAPS_DAILY].[dbo].[document] [d]
			join [HP_CAPS_DAILY].[dbo].[itemtodocument] itd on d.DocumentID = itd.DocumentID
			JOIN [HP_CAPS_DAILY].[dbo].[item] [i] ON [itd].[pmoid] = [i].[pmoid]
			left join hp_caps_daily.dbo.BusinessGroupToItem bgti on (bgti.itemid = i.itemid)
			WHERE ([d].[object_name] = @assetId)
				and (i.LocaleID = @LocaleID)
				and (
					(itd.LocaleID = @LocaleID)
					or
					(
					itd.LocaleID = 3
					and
					@LocaleID in (
								10,
								12,
								16,
								18,
								28,
								30,
								75,
								80
								)
					)
					)
			order by case when bgti.itemid is not null then 1 else 2 end, i.full_date desc
		)  AS [Limit1]
end
GO
--rollback drop procedure SQLTestGetAssociatedSKUsByAssetId

--changeset  Murugan-Dev User:SQLHPMYSG_06232016_2

IF EXISTS (select object_id from sys.objects where type='P' and name='SQL2TestCreateDeepLink_P') DROP PROCEDURE SQL2TestCreateDeepLink_P
GO

CREATE PROCEDURE [dbo].[SQL2TestCreateDeepLink_P]
(
  @link_GUID          varchar(100),
  @target_link        varchar(50), 
  @target_link_iframe varchar(200), 
  @ExpireDate         date, 
  @Disable            bit
)

AS
BEGIN
SET NOCOUNT ON 

BEGIN TRY 
BEGIN TRANSACTION

Declare	@ID int
select @ID=ID from DeepLinks where link_GUID=LTRIM(RTRIM(@link_GUID))

IF (@link_GUID IS NOT NULL and @link_GUID<>'')
BEGIN
 IF @ID IS NULL
 BEGIN
  insert into DeepLinks(link_GUID,target_link,target_link_iframe,ExpireDate,Disable) values (@link_GUID,@target_link,@target_link_iframe,@ExpireDate,@Disable)
 END
END
COMMIT TRANSACTION
END TRY 

BEGIN CATCH 
  IF @@TRANCOUNT>0

  ROLLBACK TRANSACTION

      Declare @ErrorMessage varchar(1000),@ErrorSeverity int,@ErrorState int
      SELECT @ErrorMessage=ERROR_MESSAGE(),@ErrorSeverity=ERROR_SEVERITY(),@ErrorState=ERROR_STATE()
      RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)

END CATCH 

END
GO
--rollback drop procedure SQL2TestCreateDeepLink_P