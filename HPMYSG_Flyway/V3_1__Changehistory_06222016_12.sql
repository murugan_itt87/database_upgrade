

IF EXISTS (select name from sys.objects where type='p' and name='V31FlyAATestCreateDeepLink_P') DROP PROCEDURE V31FlyAATestCreateDeepLink_P
GO


CREATE PROCEDURE V31FlyAATestCreateDeepLink_P
(
  @link_GUID          varchar(100),
  @target_link        varchar(50), 
  @target_link_iframe varchar(200), 
  @ExpireDate         date, 
  @Disable            bit
)

AS
BEGIN
SET NOCOUNT ON 
--Murugan chabegs
--updated
BEGIN TRY 
BEGIN TRANSACTION

Declare	@ID int
select @ID=ID from DeepLinks where link_GUID=LTRIM(RTRIM(@link_GUID))

IF (@link_GUID IS NOT NULL and @link_GUID<>'')
BEGIN
 IF @ID IS NULL
 BEGIN
  insert into DeepLinks(link_GUID,target_link,target_link_iframe,ExpireDate,Disable) values (@link_GUID,@target_link,@target_link_iframe,@ExpireDate,@Disable)
 END
END
COMMIT TRANSACTION
END TRY 

BEGIN CATCH 
  IF @@TRANCOUNT>0

  ROLLBACK TRANSACTION

      Declare @ErrorMessage varchar(1000),@ErrorSeverity int,@ErrorState int
      SELECT @ErrorMessage=ERROR_MESSAGE(),@ErrorSeverity=ERROR_SEVERITY(),@ErrorState=ERROR_STATE()
      RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)

END CATCH 

END
GO
