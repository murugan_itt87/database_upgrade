

IF EXISTS (select name from sys.objects where type='p' and name='FlyAATestCreateDeepLink_P') DROP PROCEDURE FlyAATestCreateDeepLink_P
GO


CREATE PROCEDURE FlyAATestCreateDeepLink_P
(
  @link_GUID          varchar(100),
  @target_link        varchar(50), 
  @target_link_iframe varchar(200), 
  @ExpireDate         date, 
  @Disable            bit
)

AS
BEGIN
SET NOCOUNT ON 
--Murugan chabegs
--updated
BEGIN TRY 
BEGIN TRANSACTION

Declare	@ID int
select @ID=ID from DeepLinks where link_GUID=LTRIM(RTRIM(@link_GUID))

IF (@link_GUID IS NOT NULL and @link_GUID<>'')
BEGIN
 IF @ID IS NULL
 BEGIN
  insert into DeepLinks(link_GUID,target_link,target_link_iframe,ExpireDate,Disable) values (@link_GUID,@target_link,@target_link_iframe,@ExpireDate,@Disable)
 END
END
COMMIT TRANSACTION
END TRY 

BEGIN CATCH 
  IF @@TRANCOUNT>0

  ROLLBACK TRANSACTION

      Declare @ErrorMessage varchar(1000),@ErrorSeverity int,@ErrorState int
      SELECT @ErrorMessage=ERROR_MESSAGE(),@ErrorSeverity=ERROR_SEVERITY(),@ErrorState=ERROR_STATE()
      RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)

END CATCH 

END
GO

IF EXISTS (select name from sys.objects where type='p' and name='FlyGet_CDS_Documents_p') DROP PROCEDURE FlyGet_CDS_Documents_p
GO

CREATE PROCEDURE [dbo].[FlyGet_CDS_Documents_p]
	@UserID int = null

AS
begin

-- Murugan added
--Alter Needed

--#########################################################
--## Start - Declare and set Variables
--#########################################################

	declare @LocaleID int = null
	declare @LocalesSelected table 
	(	LocaleID int,
		LocaleCode nvarchar(5),
		CountryCode nvarchar(2),
		CountryName nvarchar(255),
		LanguageCode nvarchar(2),
		LanguageName nvarchar(255),
		selected bit
	 )

	if @UserID is not null begin
		insert into @LocalesSelected exec HPMYSG.dbo.GetLocales_p @UserID = @UserID, @SelectedOnly = 1
	end
	
	if @UserID is null begin
		set @UserID = 203  -- set for us-en
		insert into @LocalesSelected exec HPMYSG.dbo.GetLocales_p @UserID = @UserID, @SelectedOnly = 1
	end


select	object_name
		, HTTP_URL
		, document_type
		, full_title
		, description
		, HPMYSG_Filter 
from [dbo].[CDSPlusDocuments]
where language_code in (select language_code from @LocalesSelected)


END
GO


IF EXISTS  (select name from sys.objects where type='p' and name='FlyAA_BrowseProductsByProducts_p') DROP PROCEDURE FlyAA_BrowseProductsByProducts_p
GO
CREATE PROCEDURE [dbo].[FlyAA_BrowseProductsByProducts_p]
	@UserID int,
	@IncludeHardware bit = 0,
	@IncludeConsumables bit = 0,
	@ExcludeObsolete bit = 0,
	@ExcludeRefurbished bit = 0,
	@Small_Series_PMOID int,
	@Readme bit = 0/*,

	@Rows int = 0 output,
	@Err_Num int = 0 output,
	@Err_SystemMsg nvarchar(400) = null output,
	@Err_UserMsg nvarchar (400) = null output*/
AS
begin
-- Murugan Added New
--Alter
	--#########################################################
	--## Start - Initialization
	--#########################################################
	set quoted_identifier on --allows for keywords surrounded by "s to be treated as identifiers
	set transaction isolation level read committed
	set arithabort on --terminates query if overflow or divide-by-zero error occurs
	set numeric_roundabort off --suppresses errors caused by loss of precision
	set concat_null_yields_null ON --concatenation of a string 'abc' + null yields 'abc' instead of null
	set ansi_nulls ON --specifies behavior of (=) and (<>) when used with null values.
	set ansi_padding on
	set ansi_warnings on --displays a warning if a null value appears during an aggregate function (SUM, AVG, etc)
	set nocount on --suppresses the "# rows affected" message
	--#########################################################
	--## End - Initialization
	--#########################################################

	--#########################################################
	--## Start - Return Read Me
	--#########################################################
	if @Readme = 1 
	begin
		Print('')
		goto GoodReturn
	end
	--#########################################################
	--## End - Return Read Me
	--#########################################################

  	--#########################################################
	--## Start - Declare and set Variables
	--#########################################################
	declare
	@Rows int = 0,
	@Err_Num int = 0,
	@Err_SystemMsg nvarchar(400) = null,
	@Err_UserMsg nvarchar (400) = null


	declare @ProcName nvarchar(255) -- Name of this proc
	declare @Severity int -- Local var to track severity of error
	declare @TraceDateTime datetime -- Local execution trace time

	declare @LocaleID int = null
	declare @BlockPreRelease bit = 0
	create table  #LocalesSelected (LocaleID int, LocaleCode nvarchar(5), CountryCode nvarchar(2), CountryName nvarchar(255), LanguageCode nvarchar(2),	LanguageName nvarchar(255), selected bit)
	create table  #BusinessGroupsSelected (BusinessGroupID int, Category nvarchar(50), OrderID int, selected bit)

	if @UserID is not null
	begin
		insert into #LocalesSelected
		exec GetLocales_p @UserID = @UserID, @SelectedOnly = 1

		if @LocaleID is null
			set @LocaleID = (select top 1 LocaleID from #LocalesSelected)

		insert into #BusinessGroupsSelected
		exec GetBusinessGroups_p @UserID = @UserID, @SelectedOnly = 1

		if exists (
			select top 1 u.UserId
			from [HPMYSG].[dbo].[Users] u
			join [HPMYSG].[dbo].[PreReleaseBlockList] p on right(u.email, len(u.email)-charindex('@', u.email)) = p.Domain
			where u.UserId = @UserID
			)
			set @BlockPreRelease = 1
	end

	set @localeId = coalesce(@localeId,3)

	--#########################################################
	--## End - Declare and set Variables
	--#########################################################

	--#########################################################
	--## Start - Main 
	--#########################################################
	-- Increase error severity for main execution
	select @Severity = 16

	BEGIN TRY
		select distinct * From (
			select top 100 percent
				bgti.HPMYSG_ProductType as ProductType,
				convert(int, bgti.ItemID) as ItemID,
				bgti.number,
				bgti.name,
				bgti.full_date,
				bgti.obsolete_date,
				bgti.HPMYSG_Status AS [status],
				bgti.PrimaryImageURL as [image_url_http]
			from [HP_CAPS_DAILY].[dbo].BusinessGroupToItem bgti
			join #BusinessGroupsSelected bg on bgti.[BusinessGroupID] = bg.BusinessGroupID
			join [HP_CAPS_DAILY].[dbo].Item i on bgti.ItemID = i.ItemID
			where bgti.[LocaleID] = @LocaleID
				and (@BlockPreRelease = 0 or bgti.HPMYSG_Status <> 'PreRelease')
				and (@IncludeHardware = 1 or bgti.HPMYSG_ProductType <> 'Hardware')
				and (@IncludeConsumables = 1 or bgti.HPMYSG_ProductType <> 'Consumables')
				and (@ExcludeObsolete = 0 or bgti.HPMYSG_Status <> 'Obsolete')
				and (@ExcludeRefurbished = 0 or bgti.number not like '%AR')
				and (i.Small_Series_PMOID = @Small_Series_PMOID)
				and (i.itemlevel = 7)
			order by bgti.name, bgti.number
		) a

		if @Err_Num != 0
		begin
			goto BadReturn
		end
		
		SET @Rows = @@rowcount
	END TRY
	BEGIN CATCH
		select	@Err_Num = ERROR_NUMBER(),
				@Err_SystemMsg = 'An error occured. -- ' + ERROR_MESSAGE(),
				@Err_UserMsg = 'An error occured.'
		goto BadReturn
	END CATCH	
	--#########################################################
	--## End - Main 
	--#########################################################  
        
	--#########################################################
	--## Start - Return/Error Handling
	--#########################################################
	-- Good return
	GoodReturn:
	begin
		
		-- Final check for errors
		if @Err_Num <> 0
		begin
			select @Err_UserMsg	= 'Nonzero error number encountered in GoodReturn section.',
			@Err_SystemMsg 		= 'Unknown error number caused proc to fail.'
			goto BadReturn
		end

		-- Clear the error outputs before returning them
		select	@Err_Num = 0,
				@Err_UserMsg     = null,
				@Err_SystemMsg 		= null

		return 0
	end

	-- Bad return
	BadReturn:
	begin		

		if @Err_Num = 0
			select @Err_Num = 69999
			
		if @Err_SystemMsg is null
			select @Err_SystemMsg = 'FAILED'
			
		if @Err_UserMsg is null
			select @Err_UserMsg = @Err_SystemMsg

		select @Err_SystemMsg = 'ERROR - ' + @ProcName +
				' - ' + convert(nvarchar(255), @Err_Num) +
				' - "' + @Err_SystemMsg + '"'

		if @@nestlevel = 1 -- if proc is not being used as a subprocedure
			raiserror(@Err_SystemMsg, @Severity, 1)

		return @Err_Num
	end
	--#########################################################
	--## End - Return/Error Handling
	--#########################################################
end

RETURN 0
GO


