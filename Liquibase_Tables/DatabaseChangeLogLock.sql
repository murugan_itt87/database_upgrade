--Table structure
 CREATE TABLE [dbo].[DATABASECHANGELOGLOCK](
 [ID] [int] NULL,
 [LOCKED] [int] NULL,
 [LOCKGRANTED] [datetime] NULL,
 [LOCKEDBY] [varchar](255) NULL
) 
