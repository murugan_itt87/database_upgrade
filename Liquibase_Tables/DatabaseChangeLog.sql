--Table structure
CREATE TABLE [dbo].[DATABASECHANGELOG](
 [ID] [varchar](255) NULL,
 [AUTHOR] [varchar](255) NULL,
 [FILENAME] [varchar](255) NULL,
 [DATEEXECUTED] [datetime] NULL,
 [ORDEREXECUTED] [int] NULL,
 [EXECTYPE] [varchar](10) NULL,
 [MD5SUM] [varchar](35) NULL,
 [DESCRIPTION] [varchar](255) NULL,
 [COMMENTS] [varchar](255) NULL,
 [TAG] [varchar](255) NULL,
 [LIQUIBASE] [varchar](20) NULL,
 [CONTEXTS] [nvarchar](255) NULL,
 [LABELS] [nvarchar](255) NULL,
 [DEPLOYMENT_ID] [varchar](10) NULL
) 

